# TogglReport

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Setup
Go to https://toggl.com/app/profile and copy your "API token"
Then paste it into "Toggl API Token" field on TogglReport page

### Use
To paste it into Google Sheets as tabbed values, press Ctrl-Shift-V (in first column of first empty row in a table)
