import { DateTime, Duration } from 'luxon'
import { getQueryString } from '@/lib/query'
import { DATE_FORMAT_DISPLAY } from '@/lib/constants'

export default class ClockifyClient {
  constructor (apiKey) {
    this.apiKey = apiKey
    this.headers = this.createHeaders()
  }

  createHeaders () {
    const headers = new Headers()
    headers.append('X-Api-Key', this.apiKey)
    return headers
  }

  async get (url, queryParams) {
    const queryUrl = queryParams ? `${url}?${getQueryString(queryParams)}` : url
    const response = await fetch(queryUrl, { headers: this.headers })

    if (!response.ok) {
      let error
      if (response.status === 403) {
        error = 'Error: Forbidden'
      } else {
        const json = await response.json()
        error = json.message
      }
      return [null, error]
    }

    const json = await response.json()
    return [json, undefined]
  }

  async getProfile () {
    const url = 'https://api.clockify.me/api/v1/user'
    let json
    let err
    // eslint-disable-next-line prefer-const
    [json, err] = await this.get(url)
    if (err) {
      return [null, err]
    }

    let workspaces
    // eslint-disable-next-line prefer-const
    [workspaces, err] = await this.getWorkspaces()
    if (err) {
      return [null, err]
    }

    const profile = {
      userId: json.id,
      userName: json.name,
      workspaces,
    }
    return [profile, undefined]
  }

  async getWorkspaces () {
    const url = 'https://api.clockify.me/api/v1/workspaces'
    const [json, err] = await this.get(url)
    if (err) {
      return [null, err]
    }

    const workspaces = json.map(({ id, name }) => ({ id, name }))
    return [workspaces, undefined]
  }

  async getWorkspaceClients (workspaceId) {
    const url = `https://api.clockify.me/api/v1/workspaces/${workspaceId}/clients`
    const [json, err] = await this.get(url)
    if (err) {
      return [null, err]
    }

    const clients = json.map(({ id, name }) => ({ id, name }))
    clients.sort((a, b) => a.name.localeCompare(b.name))
    return [clients, undefined]
  }

  async getWorkspaceProjects (workspaceId) {
    const url = `https://api.clockify.me/api/v1/workspaces/${workspaceId}/projects`
    const [json, err] = await this.get(url, {'page-size': 5000})
    if (err) {
      return [null, err]
    }

    const projects = json.map(({ id, name, clientId }) => ({ id, name, clientId }))
    projects.sort((a, b) => a.name.localeCompare(b.name))
    return [projects, undefined]
  }

  static transformTimesheets (_data) {
    const data = _data
      .slice()
      .reverse()
      .map(r => ({
        ...r,
        displayDate: DateTime.fromISO(r.timeInterval.start).toFormat(DATE_FORMAT_DISPLAY),
        user: r.user,
        projectId: r.projectId,
        description: r.description,
        dur: r.timeInterval.duration
          ? Duration.fromISO(r.timeInterval.duration).as('seconds')
          : -DateTime.fromISO(r.timeInterval.start).diffNow().as('seconds'),
      }))
    console.log(data)
    return data
  }

  async getTimesheets (state, dateRange) {
    const { since, until } = dateRange

    const queryParams = {
      start: since.startOf('day').setZone('UTC', { keepLocalTime: true }).toISO(),
    }
    if (state.currentProject) {
      queryParams.project = state.currentProject
    }

    if (until) {
      queryParams.end = until.endOf('day').setZone('UTC', { keepLocalTime: true }).toISO()
    }

    const url = `https://api.clockify.me/api/v1/workspaces/${state.currentWorkspace}/user/${state.userId}/time-entries`

    let data = []
    for (let page = 1; ; page += 1) {
      queryParams.page = page
      // eslint-disable-next-line no-await-in-loop
      const [json, err] = await this.get(url, queryParams)
      if (err) {
        return [null, err]
      }

      if (!json.length) {
        break
      }

      data = data.concat(json)
    }

    const reportData = ClockifyClient.transformTimesheets(data)

    return [reportData, undefined]
  }
}
