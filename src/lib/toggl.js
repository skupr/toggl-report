import { DateTime } from 'luxon'
import { getQueryString } from '@/lib/query'
import { DATE_FORMAT_DISPLAY } from '@/lib/constants'

const DATE_FORMAT_FILTER = 'yyyy-LL-dd'

export default class TogglClient {
  constructor (apiToken) {
    this.apiToken = apiToken
    this.headers = this.createHeaders()
  }

  createHeaders () {
    const headers = new Headers()
    const auth = `Basic ${btoa(`${this.apiToken}:api_token`)}`
    headers.append('Authorization', auth)
    return headers
  }

  async get (url, queryParams) {
    const queryUrl = queryParams ? `${url}?${getQueryString(queryParams)}` : url
    const response = await fetch(queryUrl, { headers: this.headers })

    if (!response.ok) {
      let error
      if (response.status === 403) {
        error = 'Error: Forbidden'
      } else {
        const json = await response.json()
        error = json.error()
      }
      return [null, error]
    }

    const json = await response.json()
    return [json, undefined]
  }

  async getProfile () {
    const url = 'https://www.toggl.com/api/v8/me'
    const [json, err] = await this.get(url)
    if (err) {
      return [null, err]
    }

    const workspaces = json.data.workspaces.map(({ id, name }) => ({ id, name }))

    const profile = {
      userId: json.data.id,
      userName: json.data.fullname,
      workspaces,
    }
    return [profile, undefined]
  }

  async getWorkspaceProjects (workspaceId) {
    if (!workspaceId) {
      return [null, undefined]
    }
    const url = `https://www.toggl.com/api/v8/workspaces/${workspaceId}/projects`
    const [json, err] = await this.get(url)
    if (err) {
      return [null, err]
    }

    const projects = json.map(({ id, name }) => ({ id, name }))
    return [projects, undefined]
  }

  static transformTimesheets (_data) {
    const data = _data
      .slice()
      .reverse()
      .map(r => ({
        ...r,
        displayDate: DateTime.fromISO(r.start).toFormat(DATE_FORMAT_DISPLAY),
        user: r.user,
        project: r.project,
        description: r.description,
        dur: r.dur / 1000,
      }))
    return data
  }

  async getTimesheets (state, dateRange) {
    const { since, until } = dateRange

    const queryParams = {
      user_agent: 'TogglReport',
      workspace_id: state.currentWorkspace,
      since: since.toFormat(DATE_FORMAT_FILTER),
      user_ids: state.userId,
    }
    if (state.currentProject) {
      queryParams.project_ids = state.currentProject
    }

    if (until) {
      queryParams.until = until.toFormat(DATE_FORMAT_FILTER)
    }

    const url = 'https://toggl.com/reports/api/v2/details'

    let data = []
    for (let page = 1; ; page += 1) {
      queryParams.page = page
      // eslint-disable-next-line no-await-in-loop
      const [json, err] = await this.get(url, queryParams)
      if (err) {
        return [null, err]
      }

      if (json.data.length) {
        data = data.concat(json.data)
      }

      if (data.length >= json.total_count) {
        break
      }
    }

    const reportData = TogglClient.transformTimesheets(data)

    return [reportData, undefined]
  }
}
