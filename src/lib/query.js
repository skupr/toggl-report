export function getQueryString (params) {
  return Object.keys(params).map(key => `${key}=${encodeURIComponent(params[key])}`).join('&')
}
