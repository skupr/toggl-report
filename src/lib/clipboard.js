export default function copyElToClipboard (el) {
  const range = document.createRange()
  const sel = window.getSelection()
  sel.removeAllRanges()
  range.selectNodeContents(el)
  sel.addRange(range)
  document.execCommand('copy')
  sel.removeAllRanges()
}
