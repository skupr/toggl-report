export function roundHalf (v) {
  return Math.round(v * 2) / 2
}
