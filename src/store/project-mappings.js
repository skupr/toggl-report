export const PROJECT_MAPPINGS = {
  SuperStores: 'SSS-Admin',
  VoxRec: 'DS-VoxRec',
  RendezVerse: 'RNDV-Backend',
  Experiments: 'DS-Experiments',
  TLDR: 'DS-TLDR',
  Zenen: 'SN-Zenen',
  Polygo: 'SN-Kippy',
}
