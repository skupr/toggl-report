import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import root from './root'

const vuexPersist = new VuexPersist({
  key: 'TogglReport',
  storage: localStorage,
})

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  ...root,
})
