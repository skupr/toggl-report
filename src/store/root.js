import { DateTime, Duration } from 'luxon'
import TogglClient from '@/lib/toggl'
import ClockifyClient from '@/lib/clockify'
import { roundHalf } from '@/lib/math'
import { PROJECT_MAPPINGS } from './project-mappings'

const stateDef = {
  reportData: [],

  apiToken: '',
  togglNotClockify: false,
  apiKey: '',
  userId: undefined,
  userName: '',
  workspaces: [],
  currentWorkspace: undefined,
  clients: [],
  currentClient: undefined,
  projects: [],
  currentProject: undefined,
  daysAgo: 2,
  showDifference: false,

  errorMessage: '',

  reportRange: 'today',

  settings: {
    projectNames: PROJECT_MAPPINGS,
  },
}

const gettersDef = {
  getDateRange (state) {
    const now = DateTime.local()

    let since
    let until

    switch (state.reportRange) {
      case 'today':
        since = now
        break
      case 'yesterday':
        since = now.minus({ days: 1 })
        until = since
        break
      case 'daysAgo':
        since = now.minus({ days: Number(state.daysAgo) })
        until = since
        break
      case 'thisWeek':
        since = now.startOf('week')
        until = now.endOf('week')
        break
      case 'prevWeek':
        since = now.minus({ weeks: 1 })
          .startOf('week')
        until = now.minus({ weeks: 1 })
          .endOf('week')
        break
      case 'thisMonth':
        since = now.startOf('month')
        until = now.endOf('month')
        break
      case 'prevMonth':
        since = now.minus({ months: 1 })
          .startOf('month')
        until = now.minus({ months: 1 })
          .endOf('month')
        break
      default:
        // same as 'today'
        since = now
        break
    }
    return {
      since,
      until,
    }
  },

  projectClientIdsMap (state) {
    return state.projects.reduce((o, a) => Object.assign(o, { [a.id]: a.clientId }), {})
  },

  projectsMap (state) {
    return state.projects.reduce((o, a) => Object.assign(o, { [a.id]: a.name }), {})
  },

  usersMap (state) {
    return { [state.userId]: state.userName }
  },

  reportTableData (state, getters) {
    if (!state.reportData) {
      return []
    }

    const dayGroups = state.reportData
      .filter(p => (
        !state.currentClient
          || getters.projectClientIdsMap[p.projectId] === state.currentClient
      ))
      .map(p => ({
        ...p,
        project: p.projectId ? getters.projectsMap[p.projectId] : p.project,
        user: p.user ? p.user : getters.usersMap[p.userId],
      }))
      .map(p => ({
        ...p,
        project: state.settings.projectNames[p.project] ? state.settings.projectNames[p.project]
          : p.project,
      }))
      .reduce((acc, r) => {
        const key = JSON.stringify([r.displayDate, r.description])

        if (acc.has(key)) {
          acc.get(key).dur += r.dur // eslint-disable-line no-param-reassign
        } else {
          acc.set(key, r)
        }

        return acc
      }, new Map())

    return [...dayGroups.values()].map(r => ({
      ...r,
      duration: roundHalf(r.dur / 3600),
      durationTime: Duration.fromMillis(r.dur * 1000).toFormat('h:mm:ss'),
      durationDifference: Math.round(r.dur / 60 - roundHalf(r.dur / 3600) * 60),
    }))
  },

  client (state) {
    return state.togglNotClockify
      ? new TogglClient(state.apiToken)
      : new ClockifyClient(state.apiKey)
  },
}

const mutations = {
  setReportData (state, v) {
    state.reportData = v
  },

  setReportRange (state, v) {
    state.reportRange = v
  },

  setApiToken (state, v) {
    state.apiToken = v
  },

  setApiKey (state, v) {
    state.apiKey = v
  },

  setProfile (state, { userId, userName }) {
    state.userId = userId
    state.userName = userName
  },

  setWorkspaces (state, v) {
    state.workspaces = v
    if (!state.currentWorkspace && state.workspaces.length) {
      state.currentWorkspace = state.workspaces[0].id
    }
  },

  setCurrentWorkspace (state, v) {
    state.currentWorkspace = v
  },

  setClients (state, v) {
    state.clients = [
      {
        id: 0,
        name: '--- All clients ---',
      },
      ...v,
    ]
  },

  setCurrentClient (state, v) {
    state.currentClient = v
  },

  setProjects (state, v) {
    state.projects = [
      {
        id: 0,
        name: '--- All projects ---',
      },
      ...v,
    ]
  },

  setCurrentProject (state, v) {
    state.currentProject = v
  },

  setDaysAgo (state, v) {
    state.daysAgo = v
  },

  setShowDifference (state, v) {
    console.log(v)
    state.showDifference = v
  },

  setError (state, v) {
    state.errorMessage = v
  },
}

const actions = {
  async loadReport ({ getters, commit, state }) {
    const dateRange = getters.getDateRange
    const [data, err] = await getters.client.getTimesheets(state, dateRange)
    commit('setError', err)
    if (err) { return }

    commit('setReportData', data)
  },

  // eslint-disable-next-line no-shadow
  async refreshProfile ({ getters, commit, dispatch }) {
    const [profile, err] = await getters.client.getProfile()
    commit('setError', err)
    if (err) { return }

    commit('setProfile', profile)
    commit('setWorkspaces', profile.workspaces)
    // getters.getcurrentwor

    dispatch('refreshClients')
    dispatch('refreshProjects')
  },

  async refreshClients ({ getters, commit, state }) {
    const [json, err] = await getters.client.getWorkspaceClients(state.currentWorkspace)
    commit('setError', err)
    if (err) { return }

    commit('setClients', json)
  },

  async refreshProjects ({ getters, commit, state }) {
    const [json, err] = await getters.client.getWorkspaceProjects(state.currentWorkspace)
    commit('setError', err)
    if (err) { return }

    commit('setProjects', json)
  },
}

export default {
  state: stateDef,
  getters: gettersDef,
  mutations,
  actions,
}
